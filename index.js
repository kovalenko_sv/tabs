"use strict";
let tab = function () {
    let tabBtn = document.querySelectorAll(".tabs-title");
    let tabContent = document.querySelectorAll(".tab");

    tabBtn.forEach(el => {
        el.addEventListener("click", selectTabNav)
    });
 
    function selectTabNav() {
        tabBtn.forEach(el => {
            el.classList.remove("active");
        });
        this.classList.add("active");
       let tabName = this.getAttribute("data-tab");
        selectTabContent(tabName);
    }

    function selectTabContent(tabName) {
        tabContent.forEach(el => {
            el.classList.contains(tabName) ? el.classList.add("active") : el.classList.remove("active");
        })
    }
}
tab();
